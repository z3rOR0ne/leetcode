#include <stdio.h>
#include <stdbool.h>

bool isPalindrome(int x) {
    if (x <= 0) {
        return false;
    }
    int y = x;
    int remainder;
    long long reverse_number = 0;
    while (y != 0) {
        remainder = y % 10;
        reverse_number = reverse_number * 10 + remainder;
        y = y / 10;
    }
    if (x == reverse_number) {
        return true;
    }
    return false;
}

int main() {
    isPalindrome(1234567899);
    return 0;
}
