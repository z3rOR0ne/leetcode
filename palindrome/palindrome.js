/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function(x) {
    var splitStringToArr = x.split('');
    var reversedArr = splitStringToArr.reverse();
    var joinedArrAsString = reversedArr.join('');
    return joinedArrAsString;
};

console.log(isPalindrome('123'));
