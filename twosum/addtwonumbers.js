// https://leetcode.com/problems/add-two-numbers/

// You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

// You may assume the two numbers do not contain any leading zero, except the number 0 itself.

// see addtwonumber1.jpg
// Input: l1 = [2,4,3], l2 = [5,6,4]
// Output: [7,0,8]
// Explanation: 342 + 465 = 807.

// Example 2:

// Input: l1 = [0], l2 = [0]
// Output: [0]
// Example 3:

// Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
// Output: [8,9,9,9,0,0,0,1]


// Constraints:

// The number of nodes in each linked list is in the range [1, 100].
// 0 <= Node.val <= 9
// It is guaranteed that the list represents a number that does not have leading zeros.


/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

var listOne = [2, 4, 3];
var listTwo= [5, 6, 4];

var addTwoNumbers = function(l1, l2) {
    var rl1 = [];
    var rl2 = [];

    for (var i = (l1.length - 1); i >= 0; i--) {
        rl1.push(l1[i]);
    }

    for (var i = (l2.length - 1); i >= 0; i--) {
        rl2.push(l2[i]);
    }

    //console.log("reversed listOne : ", rl1);
    // console.log("reversed listTwo : ", rl2);
}

addTwoNumbers(listOne, listTwo);
// Apparently linked list requires it be used as a LIST NODE, reverse(), nor join() not available, loop instead
//
// var addTwoNumbers = function(l1, l2) {
    // var reversel1 = l1.reverse();
    // var reversel2 = l2.reverse();
    // var stringFromReversel1 = reversel1.join('');
    // var stringFromReversel2 = reversel2.join('');
    // var numberFromStringFromReversel1 = parseInt(stringFromReversel1);
    // var numberFromStringFromReversel2 = parseInt(stringFromReversel2);
    // var sum = numberFromStringFromReversel1 + numberFromStringFromReversel2;
    // var sumAsString = String(sum);
    // var sumAsStringArray = Array.from(sumAsString);
    // finalNumArray  = [];
    // for (var i = 0; i < sumAsStringArray.length; i++) {
        // finalNumArray.push(Number(sumAsStringArray[i]));
    // }

    // return finalNumArray;

// };
