// https://leetcode.com/problems/two-sum/

// Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

// You may assume that each input would have exactly one solution, and you may not use the same element twice.

// You can return the answer in any order.

var myArr = [2, 7, 11, 15];

var twoSum = function(nums, target) {
    var sumArr = [];
    var sum;
    for (var i = 0; i < nums.length; i++) {
        for (var j = i + 1; j < nums.length; j++) {
            sum = nums[i] + nums[j];
            if (sum === target) {
               sumArr.push(i, j)
            }
        }
    }
    return sumArr;
}

twoSum(myArr, 13);
